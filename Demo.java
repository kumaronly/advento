package hibernet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Demo {
	public static void main(String[] args) {
		EntityManagerFactory factory=Persistence.createEntityManagerFactory("advento");
		EntityManager manager=factory.createEntityManager();
		EntityTransaction transaction=manager.getTransaction();
		
		Employee4 e4=new Employee4();
		e4.setEid(1);
		e4.seteName("kumar");
		e4.seteAge("25");
		e4.setePhone("7205014047");
		transaction.begin();
		manager.persist(e4);
		transaction.commit();
	}
}
