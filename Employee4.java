package hibernet;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employee4 {
	@Id
	int eid;
	String eName;
	String eAge;
	String ePhone;
	
	//default constructor
	public Employee4() {
		super();
	}
	
//	parameterised constructor
	public Employee4(int eid, String eName, String eAge, String ePhone) {
		super();
		this.eid = eid;
		this.eName = eName;
		this.eAge = eAge;
		this.ePhone = ePhone;
	}

//	getter and setter
	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public String geteAge() {
		return eAge;
	}

	public void seteAge(String eAge) {
		this.eAge = eAge;
	}

	public String getePhone() {
		return ePhone;
	}

	public void setePhone(String ePhone) {
		this.ePhone = ePhone;
	}

	
	
	
	
	

}
